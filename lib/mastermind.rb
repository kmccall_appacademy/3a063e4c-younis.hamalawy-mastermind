class Code
  attr_reader :pegs

   PEGS = { "R" => :Red, "G" => :green, "B" => :blue, "Y" => :yellow, "O" => :orange, "P" => :purple }

   def initialize(pegs)
     @pegs = pegs
   end



   def self.parse(str)
     pegs_arr = PEGS.keys
     str_split = str.upcase.split("")
     pegs = str_split.map do |el|
       raise "Not a valid color" unless pegs_arr.include?(el)
       el
     end
     Code.new(pegs)
   end

   def self.random
     secret_code = ''
     4.times { secret_code << PEGS.keys.sample }
     self.new(secret_code)
   end

   def [](i)
     self.pegs[i]
   end

   def exact_matches(guess)
     counter = 0
     guess.pegs.each_index do |i|
       counter += 1 if guess.pegs[i] == @pegs[i]
     end
     counter
   end

   def near_matches(guess)
     counter = 0
     PEGS.keys.each do |ch|
       counter += [@pegs.count(ch), guess.pegs.count(ch)].min
     end
     counter - exact_matches(guess)
   end

   def ==(guess)
     return false unless guess.class == Code
     @pegs == guess.pegs
   end

end

class Game
  attr_reader :secret_code

  def initialize(secret_code = Code.random)
    @secret_code = secret_code# || Code.random
    #Code.new(secret_code)
  end

  def get_guess
    puts "Please enter a guess:"
    Code.parse(gets.chomp)
  end

  def display_matches(guess)
    puts "near matches: #{@secret_code.near_matches(guess)}"
    puts "exact matches: #{@secret_code.exact_matches(guess)}"
  end

  def play

  end

end
